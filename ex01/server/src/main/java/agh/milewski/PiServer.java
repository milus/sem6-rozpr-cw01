package agh.milewski;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class PiServer {

    private final int port;

    public PiServer(int port) {
        this.port = port;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println(
                    "Please specify port number");
            System.exit(1);
        }
        int port = Integer.parseInt(args[0]);
        PiServer.runOn(port);
    }

    private static void runOn(int port) {
        new PiServer(port).run();
    }

    private void run() {
        try (
                ServerSocket serverSocket = new ServerSocket(port);
                Socket clientSocket = serverSocket.accept();

                DataOutputStream out =
                        new DataOutputStream(clientSocket.getOutputStream());
                DataInputStream in = new DataInputStream(
                        clientSocket.getInputStream());
        ) {
            while (true) {
                long request = parseInput(in);
                System.out.println("Received: " + request);
                byte result = new PiProtocol().processInput(request);
                System.out.println("Send: " + result);
                sendResult(out, result);
            }
        } catch (EOFException e) {
            System.out.println("No more data to receive");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private long parseInput(DataInputStream in) throws IOException {
        int available = in.readInt();
        switch (available) {
            case 0:
                throw new EOFException();
            case 1:
                return in.readByte();
            case 2:
                return in.readShort();
            case 4:
                return in.readInt();
            case 8:
                return in.readLong();
            default:
                throw new IllegalStateException("Illegal number of bytes: " + available);
        }
    }

    private void sendResult(DataOutputStream out, byte result) throws IOException {
        out.write(result);
    }
}
