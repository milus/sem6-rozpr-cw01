package agh.milewski;

public class PiProtocol {
    public byte processInput(long request) {
        return (byte) ((request + 1) % 255);
    }
}
