package agh.milewski;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class PiClient {
    public static void main(String[] args) throws IOException {

        if (args.length != 2) {
            System.err.println(
                    "Please specify host name and port number");
            System.exit(1);
        }

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);

        try (
                Scanner scanner = new Scanner(System.in);
                Socket socket = new Socket(hostName, portNumber);
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                DataInputStream in = new DataInputStream(socket.getInputStream());
        ) {
            String number;
            while ((number = scanner.nextLine()) != null) {
                try {
                    long l = Long.parseLong(number);
                    if (l < Byte.MAX_VALUE) {
                        out.writeInt(1);
                        out.writeByte((byte) l);
                    } else if (l < Short.MAX_VALUE) {
                        out.writeInt(2);
                        out.writeShort((short) l);
                    } else if (l < Integer.MAX_VALUE) {
                        out.writeInt(4);
                        out.writeInt((int) l);
                    } else {
                        out.writeInt(8);
                        out.writeLong(l);
                    }
                    System.out.println(in.readByte());
                } catch (NumberFormatException e) {
                    System.err.println("Please type in number");
                }

            }
        } catch (NoSuchElementException e) {
            System.out.println("Finished");
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                    hostName);
            System.exit(1);
        }
    }
}
