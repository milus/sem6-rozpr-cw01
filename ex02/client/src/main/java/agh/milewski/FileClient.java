package agh.milewski;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class FileClient {
    public static void main(String[] args) throws IOException {

        if (args.length != 3) {
            System.err.println(
                    "Please specify host name, port number and filename.");
            System.exit(1);
        }

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);

        try (
                Socket socket = new Socket(hostName, portNumber);
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
        ) {


            File file = new File(args[2]);

            if (file.exists()) {
                String filename = file.getName();
                out.writeByte(filename.length());
                out.writeChars(filename);

                copyDataFromFileToSocket(file, out);
            }



        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                    hostName);
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static void copyDataFromFileToSocket(File file, DataOutputStream socketOutput) throws IOException {
        DataInputStream input = new DataInputStream(new FileInputStream(file));
        int data;
        while ((data = input.read()) != -1) {
            socketOutput.write(data);
        }
    }
}
