package agh.milewski;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.*;

public class FileServer {

    private final int port;

    public FileServer(int port) {
        this.port = port;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println(
                    "Please specify port number");
            System.exit(1);
        }
        int port = Integer.parseInt(args[0]);
        FileServer.runOn(port);
    }

    private static void runOn(int port) {
        new FileServer(port).run();
    }

    private void run() {
        try (
                ServerSocket serverSocket = new ServerSocket(port);
                Socket clientSocket = serverSocket.accept();
                DataInputStream socketInput = new DataInputStream(
                        clientSocket.getInputStream());
        ) {
            String fileName = readFilename(socketInput);
            System.out.println(fileName);

            createIfDoesNotExistsServerDirectory();
            File file = openOrCreateFile(fileName);
            copyDataFromSocketToFile(socketInput, file);


        } catch (EOFException e) {
            System.out.println("No more data to receive");
        } catch (FileAlreadyExistsException e) {
            System.out.println("File currently exists. Exiting.");
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void createIfDoesNotExistsServerDirectory() {
        File theDir = new File("server_files");

        // if the directory does not exist, create it
        if (!theDir.exists()) {
            try{
                theDir.mkdir();
            } catch(SecurityException se){
                //handle it
            }
        }
    }

    private String readFilename(DataInputStream in) throws IOException {
        byte nameLength = in.readByte();
        StringBuilder stringBuilder = new StringBuilder(nameLength);
        for (int i = 0; i < nameLength; i++) {
            char c = in.readChar();
            stringBuilder.append(c);
        }
        return stringBuilder.toString();
    }

    private File openOrCreateFile(String fileName) throws IOException {
        Path path = Paths.get("server_files", fileName);
        System.out.println(path.toString());
        File file = new File(path.toString());
        if (!file.exists()) {
            file.createNewFile();
        } else {
            new PrintWriter(file).close();
        }
        return file;
    }

    private void copyDataFromSocketToFile(DataInputStream socketInput, File file) throws IOException {
        DataOutputStream fileOutput = new DataOutputStream(new FileOutputStream(file));
        int data;
        while ((data = socketInput.read()) != -1) {
            fileOutput.write(data);
        }
    }
}
