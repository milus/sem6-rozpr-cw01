package agh.milewski.exception;

public class SystemConsoleNotExistingException extends Exception {
    public SystemConsoleNotExistingException() {
        super("System console does not exist");
    }
}
