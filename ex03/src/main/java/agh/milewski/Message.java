package agh.milewski;

import agh.milewski.exception.IllegalChecksumException;
import com.google.common.base.Preconditions;

import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketAddress;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

public class Message{
    private static final int SIZE = 106;

    private final String nickname;

    private final LocalDateTime sentTime;
    private final String msg;
    private final int checkSum;
    private Message(String nickname, LocalDateTime sentTime, String msg) {
        this.nickname = nickname;
        this.sentTime = sentTime;
        this.msg = msg;
        checkSum = hashCode();
    }

    public static Message create(String nickname, String msg) {
        String message = trim(Preconditions.checkNotNull(msg));
        return new Message(Preconditions.checkNotNull(nickname), LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS), message);
    }

    public String nickname() {
        return nickname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (msg != null ? !msg.equals(message.msg) : message.msg != null) return false;
        if (nickname != null ? !nickname.equals(message.nickname) : message.nickname != null) return false;
        if (sentTime != null ? !sentTime.equals(message.sentTime) : message.sentTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nickname != null ? nickname.hashCode() : 0;
        result = 31 * result + (sentTime != null ? sentTime.hashCode() : 0);
        result = 31 * result + (msg != null ? msg.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return sentTime + " " + nickname + ": " + msg;
    }

    private static String trim(String message) {
        if (message.length() > 20) {
            System.out.println("Your message has to be amx 20 characters long. Message will be trimmed");
            return message.substring(0, 20);
        }
        return message;
    }

    /**
     * Length and structure of array is constant:
     * 4B - nickname length
     * 6*2B - nickname
     * 4B - sentTime length
     * 19*2B - sentTime
     * 4B - msg length
     * 20*2B - msg
     * 4B - checksum
     * @return New bytes array with length =
     * @throws IOException
     */
    public byte[] bytes() throws IOException {
        ByteArrayOutputStream b = new ByteArrayOutputStream(SIZE);
        DataOutputStream d = new DataOutputStream(b);
        d.writeInt(nickname.length());
        d.writeChars(nickname);
        d.writeInt(sentTime.toString().length());
        d.writeChars(sentTime.toString());
        d.writeInt(msg.length());
        d.writeChars(msg);
        d.writeInt(checkSum);
        return Arrays.copyOf(b.toByteArray(), SIZE);
    }

    public void send(MulticastSocket socket, SocketAddress group, int port) throws IOException {
        byte[] buf = bytes();
        DatagramPacket datagramPacket = new DatagramPacket(buf, buf.length, group);
        socket.send(datagramPacket);
    }

    public void send(MulticastSocket socket, InetAddress group, int port) throws IOException {
        byte[] buf = bytes();
        DatagramPacket datagramPacket = new DatagramPacket(buf, buf.length, group, port);
        socket.send(datagramPacket);
    }

    public static Message receive(MulticastSocket socket, String ip, int port) throws IOException, IllegalChecksumException {
        Preconditions.checkNotNull(socket);
        Preconditions.checkNotNull(ip);

        DatagramPacket packet = new DatagramPacket(new byte[SIZE], SIZE);
        socket.receive(packet);
        byte[] data = packet.getData();

        ByteArrayInputStream b = new ByteArrayInputStream(data);
        DataInputStream d = new DataInputStream(b);

        int nicknameLength = d.readInt();
        StringBuilder sb = new StringBuilder(nicknameLength);
        for (int i = 0; i < nicknameLength; i++) {
            sb.append(d.readChar());
        }
        String nickname = sb.toString();
        int timeLength = d.readInt();
        sb = new StringBuilder(timeLength);
        for (int i = 0; i < timeLength; i++) {
            sb.append(d.readChar());
        }
        LocalDateTime time = LocalDateTime.parse(sb.toString());
        int msgLength = d.readInt();
        sb = new StringBuilder(msgLength);
        for (int i = 0; i < msgLength; i++) {
            sb.append(d.readChar());
        }
        String msg = sb.toString();

        Message message = new Message(nickname, time, msg);
        int checkSum = d.readInt();
        if (checkSum != message.checkSum) {
            throw new IllegalChecksumException();
        }
        return message;
    }
}
