package agh.milewski;

import agh.milewski.exception.IllegalChecksumException;

import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class ChatClient {
    public static final String IP = "224.0.0.2";
    private final String nickname;
    private final int port;
    private final ExecutorService service = Executors.newFixedThreadPool(2);
    private Runnable sendingThread;
    private final AtomicBoolean wantToExit = new AtomicBoolean(false);

    public ChatClient(String nickname, int port) {
        this.nickname = nickname;
        this.port = port;
    }

    public static void start(String nickname, int port) {
        ChatClient chatClient = new ChatClient(nickname, port);
        chatClient.startSendingThread();
        chatClient.startReceivingThread();
        chatClient.service.shutdown();
    }

    private void startSendingThread() {
        sendingThread = () -> {
            try (
                    Scanner scanner = new Scanner(System.in);
                    MulticastSocket socket = new MulticastSocket(port)
            ) {

                InetAddress group = InetAddress.getByName(IP);
                socket.joinGroup(group);

                String msg;
                while ((msg = scanner.nextLine()) != null) {
                    Message message = Message.create(nickname, msg);
                    message.send(socket, group, port);
                }

            } catch (NoSuchElementException e) {
                System.out.println("Exiting from program");
                wantToExit.set(true);
            } catch (Exception e) {
                System.err.println(e.getMessage());
                wantToExit.set(true);
            }

        };
        service.submit(sendingThread);
    }

    private void startReceivingThread() {
        service.submit(() -> {
            try (
                    MulticastSocket socket = new MulticastSocket(port)
            ) {
                InetAddress group = InetAddress.getByName(IP);    //todo: set correct address
                socket.joinGroup(group);
                socket.setSoTimeout(2000);

                while (true) {
                    if (wantToExit.get()) {
                        return;
                    }
                    try {
                        Message msg = Message.receive(socket, IP, port);
                        if (!msg.nickname().equals(nickname)) {
                            System.out.println(msg.toString());
                        }
                    } catch (SocketTimeoutException ignored) {

                    } catch (IllegalChecksumException e) {
                        System.out.println("Illegal checksum of message.");
                    }
                }

            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        });
    }
}
