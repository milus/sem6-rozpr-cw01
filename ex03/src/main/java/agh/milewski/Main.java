package agh.milewski;

import agh.milewski.exception.InvalidArgsException;

public class Main {
    public static void main(String[] args) {
        try {
            String nickname = getNameFromArgs(args);
            int port = getPortFromArgs(args);
            ChatClient.start(nickname, port);
        } catch (InvalidArgsException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }

    private static String getNameFromArgs(String[] args) throws InvalidArgsException {
        if (args.length < 1) {
            throw new InvalidArgsException("Please specify nickname");
        }
        String nickname = args[0];
        if (nickname.length() > 6) {
            throw new InvalidArgsException("Nickname has to be max 6 chars length");
        }
        return nickname;
    }

    private static int getPortFromArgs(String[] args) throws InvalidArgsException {
        if (args.length < 2) {
            throw new InvalidArgsException("Please specify port number");
        }
        return Integer.parseInt(args[1]);
    }
}
